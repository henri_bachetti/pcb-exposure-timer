
#include <LowPower.h>
#include <EEPROM.h>
#include <Keypad.h>
#include <LiquidCrystal.h>
#include <MsTimer2.h>

#define RELAY_PIN           6
#define BUZZER_PIN          10
#define ROWS                4
#define COLS                3
#define METHOD_SEC          1
#define METHOD_HMS          2

struct config
{
  unsigned long toElapse = 0UL;
  int method;
  int buzzer;
};

struct eeprom_data
{
  struct config configuration;
  unsigned long crc;
};

char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
};

byte rowPins[ROWS] = {14, 15, 16, 17};
byte colPins[COLS] = {7, 8, 9};

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);
char keyBuf[10];
int keyIndex = 0;
const char *prompt;
int buzzer = 0;
struct eeprom_data eepromData;

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
unsigned long toElapse = 0UL;

void InterruptTimer2(void)
{
  displayTime();
  if (toElapse > 0UL) {
    Serial.println(toElapse);
    digitalWrite(RELAY_PIN, 1);
    toElapse--;
  }
  else {
    digitalWrite(RELAY_PIN, 0);
    if (toElapse == 0) {
      MsTimer2::stop();
      buzzer = 1;
    }
  }
}

unsigned long eeprom_crc(int sz) {

  const unsigned long crc_table[16] = {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };

  unsigned long crc = ~0L;

  for (int index = 0 ; index < sz  ; ++index) {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}

void printConfig(void)
{
  Serial.print("eepromData.configuration.toElapse: "); Serial.println(eepromData.configuration.toElapse);
  Serial.print("eepromData.configuration.method: "); Serial.println(eepromData.configuration.method);
  Serial.print("eepromData.configuration.buzzer: "); Serial.println(eepromData.configuration.buzzer);
}

void saveConfig(void)
{
  Serial.println("saving configuration");
  printConfig();
  EEPROM.put(0, eepromData.configuration);
  eepromData.crc = eeprom_crc(sizeof(struct config));
  Serial.print("eepromData.crc: 0x"); Serial.println(eepromData.crc, HEX);
  EEPROM.put(sizeof(struct config), eepromData.crc);
}

void displayTimeField(int withPrompt)
{
  int offset = strlen(prompt)+1;
  int hours, minutes, seconds;
  char buf[10];

  if (withPrompt) {
    lcd.setCursor(0, 0);
    lcd.print(prompt);
  }
  if (eepromData.configuration.method == METHOD_SEC) {
    seconds = atoi(keyBuf);
    sprintf(buf, "%04d", seconds);
    lcd.setCursor(offset, 0);
    lcd.print(buf);
  }
  else {
    Serial.print("keyBuf: "); Serial.println(keyBuf);
    Serial.print("len: "); Serial.println(keyIndex);
    hours = atoi(substr(keyBuf, max(0, keyIndex-6), max(0, keyIndex-4)));
    minutes = atoi(substr(keyBuf, max(0, keyIndex-4), max(0, keyIndex-2)));
    seconds = atoi(substr(keyBuf, max(0, keyIndex-2), keyIndex));
    sprintf(buf, "%02d:%02d:%02d", hours, minutes, seconds);
    lcd.setCursor(offset, 0);
    lcd.print(buf);
  }
}

void displayTime(void)
{
  char buf[10];
  int hours, minutes, seconds;
  unsigned long tmp = toElapse;

  hours = tmp / 3600;
  tmp = tmp % 3600;
  minutes = tmp / 60;
  seconds = tmp % 60;
  sprintf(buf, "%02d:%02d:%02d", hours, minutes, seconds);
  lcd.setCursor(4, 1);
  lcd.print(buf);
}

const char *substr(char *s, int from, int to)
{
  static char tmp[10];
  int len = strlen(s);
  memcpy(tmp, s+from, to-from);
  tmp[to-from] = 0;
  return tmp;
}

void buzz(long frequency, long length) {
  if (eepromData.configuration.buzzer) {
    long delayValue = 1000000 / frequency / 2;
    long numCycles = frequency * length / 1000;
    for (long i = 0 ; i < numCycles ; i++)
    {
      digitalWrite(BUZZER_PIN, HIGH);
      delayMicroseconds(delayValue);
      digitalWrite(BUZZER_PIN, LOW);
      delayMicroseconds(delayValue);
    }
  }
}

char ask(char *question, char *choice, char *keys)
{
  int count;
  char key;

  lcd.clear();
  lcd.print(question);
  lcd.setCursor(0, 1);
  lcd.print(choice);
  while (1) {
    key = keypad.waitForKey();
    for (count = 0 ; count < strlen(keys) ; count++) {
      if (key == keys[count]) {
        return key;
      }
    }
  }
}

//
// SETUP
//
void setup(void)
{
  pinMode(13, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  Serial.begin(115200);

  keypad.setHoldTime(4);
  keypad.setDebounceTime(4);
  Serial.print("eepromData.size: "); Serial.println(EEPROM.length());
  EEPROM.get(0, eepromData);
  if (eeprom_crc(sizeof(struct config)) != eepromData.crc) {
    Serial.println("invalid eeprom CRC");
    eepromData.configuration.toElapse = 10;
    eepromData.configuration.method = METHOD_SEC;
    eepromData.configuration.buzzer = 1;
    saveConfig();
  }
  printConfig();
  Serial.print("eepromData.crc: 0x"); Serial.println(eepromData.crc, HEX);
  lcd.begin(16, 2);
  if (keypad.getKey() != NO_KEY) {
    char key;
    Serial.println("entering configuration");
    key = ask("BUZZER:", "1=ON 0=0FF", "01");
    eepromData.configuration.buzzer = key - '0';
    key = ask("METHOD:", "1=SEC 2=HMS", "12");
    eepromData.configuration.method = key - '0';
    saveConfig();
  }
  if (eepromData.configuration.method == METHOD_SEC) {
    prompt = "SECONDS:";
    sprintf(keyBuf, "%d", eepromData.configuration.toElapse);
  }
  else {
    int hours, minutes, seconds;
    unsigned long tmp = eepromData.configuration.toElapse;
  
    prompt = "HHMMSS:";
    hours = tmp / 3600;
    tmp = tmp % 3600;
    minutes = tmp / 60;
    seconds = tmp % 60;
    sprintf(keyBuf, "%02d:%02d:%02d", hours, minutes, seconds);
  }
  keyIndex = strlen(keyBuf);
  lcd.clear();
  displayTimeField(1);
}

void loop(void)
{
  if (buzzer && millis() % 1000 == 0) {
    buzz(2500, 200);
  }
  char key = keypad.getKey();
  if (key != NO_KEY) {
    Serial.println(key);
    Serial.print("Key: 0x");
    Serial.println(key, HEX);

    if (buzzer) {
      buzzer = 0;
      return;
    }
    buzz(400, 50);
    switch (key) {
    case '#':
      Serial.print("toElapse: "); Serial.println(toElapse);
      Serial.print("keyBuf: "); Serial.println(keyBuf);
      Serial.print("len: "); Serial.println(keyIndex);
      if (toElapse == 0) {
        if (keyIndex > 0) {
          if (eepromData.configuration.method == METHOD_SEC) {
            toElapse = atoi(keyBuf);
          }
          else {
            if (keyIndex > 0) {
              const char *hours, *minutes, *seconds;
              Serial.print("keyBuf: "); Serial.println(keyBuf);
              Serial.print("len: "); Serial.println(keyIndex);
              seconds = substr(keyBuf, max(0, keyIndex-2), keyIndex);
              Serial.print("seconds: "); Serial.println(seconds);
              toElapse = atol(seconds);
              minutes = substr(keyBuf, max(0, keyIndex-4), max(0, keyIndex-2));
              Serial.print("minutes: "); Serial.println(minutes);
              toElapse += atol(minutes) * 60;
              hours = substr(keyBuf, max(0, keyIndex-6), max(0, keyIndex-4));
              Serial.print("hours: "); Serial.println(hours);
              toElapse += atol(hours) * 3600;
              Serial.print("toElapse: "); Serial.println(toElapse);
            }
          }
          eepromData.configuration.toElapse = toElapse;
          saveConfig();
          MsTimer2::set(1000, InterruptTimer2);
          MsTimer2::start();
        }
        else {
          toElapse = 0;
          lcd.clear();
          displayTimeField(1);
        }
      }
      else {
        digitalWrite(RELAY_PIN, 0);
        MsTimer2::stop();
        toElapse = 0;
        displayTime();
      }
      break;
    case '*':
      if (keyIndex > 0) {
        keyBuf[--keyIndex] = 0;
        displayTimeField(0);
      }
      break;
    default:
      if (eepromData.configuration.method == METHOD_SEC) {
        if (keyIndex < 4) {
          keyBuf[keyIndex++] = key;
          displayTimeField(0);
        }
      }
      else {
        if (keyIndex < 6) {
          keyBuf[keyIndex++] = key;
          displayTimeField(0);
        }
      }
      break;
    }
  }
}

