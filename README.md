# PCB EXPOSURE COUNTDOWN TIMER

The purpose of this page is to explain step by step the realization of a PCB expusure countdown timer based on ARDUINO NANO.

The board uses the following components :

 * an ARDUINO NANO
 * a 2x16 LCD display
 * a JQC-3F or SRD05VDC-SL-C relay
 * a buzzer
 * a matrix keyboard
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/03/minuterie-pour-insoleuse-de-pcb.html